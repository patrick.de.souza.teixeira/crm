## Introdução ao Projeto

Projeto de estudos utilizando biblioteca SPRING para criação de um CRM "Customer Relationship Management" (Gestão de Relacionamento com o Cliente)

## Tecnologias 

- [JUnit5](https://junit.org/junit5/) - Plataforma para construção e execução de testes, de modo que o JUnit 5 é composto por diversos módulos com papéis diferentes (ao invés de “um único framework”):

- [H2 database](https://www.h2database.com/html/main.html) - Banco em memória

- [Swagger](https://swagger.io/) - Trata-se de uma aplicação open source que auxilia desenvolvedores nos processos de definir, criar, documentar e consumir APIs REST.

- [Spring Boot](https://spring.io/projects/spring-boot) - O Spring Boot é uma ferramenta que visa facilitar o processo de configuração e publicação de aplicações que utilizem o ecossistema Spring..

- [Lombok](https://projectlombok.org/) - O Lombok é uma biblioteca Java focada em produtividade e redução de código boilerplate que, por meio de anotações adicionadas ao nosso código, ensinamos o compilador (maven ou gradle) durante o processo de compilação a criar código Java.


## Pré requisitos
 - [Maven](https://maven.apache.org/) - Ferramenta de automação de compilação utilizada primariamente em projetos Java.
