package com.tskdeveloper.crm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tskdeveloper.crm.model.ClientModel;
import com.tskdeveloper.crm.repository.ClientRepository;

@RestController
@RequestMapping("/clients")
public class ClientController {

	@Autowired
	private ClientRepository clientRepository;

	@GetMapping
	public List<ClientModel> listAllClients() {
		return clientRepository.findAll();
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ClientModel add(@RequestBody ClientModel client) {
		return clientRepository.save(client);
	}

	@DeleteMapping
	public ResponseEntity<?> deleteById(@RequestBody ClientModel client) {
		return clientRepository.findById(client.getId()).map(record -> {
			clientRepository.deleteById(client.getId());
			return ResponseEntity.ok().build();
		}).orElse(ResponseEntity.notFound().build());
	}
}
