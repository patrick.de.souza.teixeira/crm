package com.tskdeveloper.crm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tskdeveloper.crm.model.ClientModel;

@Repository
public interface ClientRepository extends JpaRepository<ClientModel, Long> {

}
